#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>


/*
 * Vypsání nápovědy
 */
void print_help(){
	printf("Pouziti: \n ./proj + [nazev souboru s grafem]\n");
}
/*
 * Alokace
 */
int* allocate_mem(int*** arr, int n, int m) {
  *arr = (int**)malloc(n * sizeof(int*));
  int *arr_data = malloc( n * m * sizeof(int));
  for(int i=0; i<n; i++) {
     (*arr)[i] = arr_data + i * m ;
  }
  return arr_data; //free point
} 

/* 
 * Dealokace
 */

void deallocate_mem(int*** arr){
    free(*arr);
    *arr=NULL;
}

/*
 * Ziskani velikosti grafu
 */

int count_lines(char *filename) {
	int lines = 0;
	FILE *file = fopen(filename,"r");
	if (file == NULL) {
		fprintf(stderr, "Chyba otevirani souboru\n");
		exit(-2);
	}
	char ch;
	while(!feof(file)) {
		ch = fgetc(file);
		if(ch == '\n') {
			lines++;
		}
	}
	if (lines > 0) {
		lines++;
	}
	return lines;

}

/*
 * Vypis grafu ve forme tabulky
 */

void printout_matrix(int **matrix, int rows, int columns) {
	int i, j;
	printf("  ");
	for (i = 0; i < columns; i++) {
		printf("%c ", i + 65);
	}
	printf("\n");
	
	for (i = 0; i < rows ; i++) {	
		printf("%c ", i + 65);
		for (j = 0; j < columns; j++) {
			printf("%d ", matrix[i][j]);
		}
		printf("\n");
		j = 0;
	}
}

/*
 * Nacteni do matice
 */

int **get_matrix(char *filename, int size) {
	
	
	char buffer[1024] ;
    char *record, *line;
    int i=0,j=0;
	int **matrix;

	//alokace
    allocate_mem(&matrix, size, size);

	//kontrola alokace
	if (matrix == NULL) {
		fprintf(stderr, "Chyba pameti\n");
		exit(-3);
	}

	//otevreni vstupniho souboru
	FILE *file = fopen(filename,"r");
	if (file == NULL) {
		fprintf(stderr, "Chyba otevirani souboru\n");
		exit(-2);
	}

	//ulozeni do matice
	while((line = fgets(buffer, sizeof(buffer), file)) != NULL) {
		record = strtok(line, " ,;");
		while(record != NULL) {
			matrix[i][j] = atoi(record);
			record = strtok(NULL, " ,;");
			j++;
		}
		i++;
		j = 0;
	}

	return matrix;
}

/*
 *	Kontrola matice
 */ 
bool check_matrix(int **matrix, int size) {
	int i, j;
	
	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			if (matrix[i][j] != matrix[j][i]) {
				return false;
			}
		}
	}
	return true;
}

/*
 * Ziskani barev a chromatickeho cisla
 */

void color_matrix(int **matrix, int size) {
	int i, j;
	int colors[size];
	
	for (i = 0; i < size; i++) {
		colors[i] = 0;
	}
	int chromatic = 1;
	
	//TODO: vytvorit algoritmus
	for (i = 1; i < size; i++) {
		for (j = 0; j < i; j++) {
			if (matrix[i][j] == 0) {
				colors[i] = colors[j];
				int k;
				for (k = 0; k < j; k++) {
					if ((matrix[i][k] == 1) && (colors[k] == colors[i])) {
						colors[i]++;
					}
				}
			} else {
				if (colors[i] == colors[j]) {
					colors[i]++;
				}
			}
		}
	}
	for (i = 0; i <size; i++) {
		printf("Uzel %c: barva %d\n", i + 65, colors[i]);
		if (colors[i] >= chromatic) {
			chromatic = colors[i] + 1;
		}
	}
	
	printf("Chromaticke cislo: %d\n", chromatic);
}

int main(int argc, char *argv[]){
	if (argc == 1){
		print_help();
	}else if (argc == 2) {
		if(strcmp(argv[1], "/h") == 0 || strcmp(argv[1], "/help") == 0 ){
			print_help();
		}else{
			int size = count_lines(argv[1]);
			printf("Pocet uzlu grafu: %d\n", size);
			if (size > 0) {
				int** matrix = get_matrix(argv[1], size);
				if (!check_matrix(matrix, size)) {
					fprintf(stderr, "Zly format grafu\n");
					return(-2);
				}
				color_matrix(matrix, size);
				printout_matrix(matrix, size, size);
                deallocate_mem(&matrix);
			} else {
				fprintf(stderr, "Graf je prazdny\n");
				return(-1);
			}
		}

	} else {
		fprintf(stderr, "Zadny soubor\n");
		print_help();
		return -1;
	}
	return 0;
}