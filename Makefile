CC=gcc  
CFLAGS=-std=c99 -Wall -Wextra -Werror
PROJ=proj
GENERATOR=graph_gen


hello: $(PROJ).c
	-$(CC) $(CFLAGS) $(PROJ).c -o $(PROJ)
	-$(CC) $(CFLAGS) $(GENERATOR).c -o $(GENERATOR)

clean:
	rm -f $(PROJ)
	rm -f $(GENERATOR)

