/*
 * Generator grafu (graph_gen.c)
 * IAL - 2017/2018, nahradni projekt, obarveni grafu 
 * xturek05
 * generator X grafov (podle argumentu) postupne od 0 az po X 
 * 4.12.2017 - vytvoreni
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


/*
 * Napoveda
 */
void print_help(){
	printf("Pouziti: ./graph_gen + N\n");
}

/* 
 * Generace cisel - 0 a 1 
 */
int get_random () {
	return rand() % 2;
}

/*
 * Generace soubru
 *
 * pokud existuju, prepisu se
 */

int gen_graphs(int n) {
	FILE *file;
	int i;
	for (i = 0; i < n; i++) {
		
		char filename[sizeof "graph"];

		sprintf(filename, "graph%d.csv", i);
		file = fopen(filename, "wb");
		if (file == NULL) {
			return -1;
		}
		int j, k;
		int matrix[i][i];
		
		//generace tabulky
		for (j = 0; j < i; j++) {
			for (k = 0; k < i; k++) {
				if (k == j) {
					matrix[j][k] = 0; 
				} else {
					int x = get_random();
					matrix[j][k] = x;
					matrix[k][j] = x;
				}
			}
		}
		//zapis do souboru
		for (j = 0; j < i; j++) {
			for (k = 0; k < i; k++) {
				if ((k + 1) != i) {
					fprintf(file, "%d,", matrix[j][k]);
				} else {
					if(j==i-1){
						fprintf(file, "%d", matrix[j][k]);
					}else{
						fprintf(file, "%d\n", matrix[j][k]);
					}
				}
			}
		}
		printf("Graf %d vytvoren\n", i);
		fclose(file);
	}
	return n;
}

/*
 * Main
 */

int main(int argc, char *argv[]) {
	
	int n;
	
	if (argc == 1) {
		print_help();
	} else if (argc == 2) {
		if(strcmp(argv[1], "/h") == 0 || strcmp(argv[1], "/help") == 0 ){
			print_help();
		} else {
			n =  strtod(argv[1], '\0');
			if (n <= 0) {
				fprintf(stderr, "Chyba argumentu: N je mensi nebo roven 0\n");
				return -1;
			}
			if (gen_graphs(n) >= 0) {
				printf("Vsechy grafy uspesne vytvoreny\n");
			} else {
				fprintf(stderr, "Chyba pri vytvarani grafu\n");
			}
		}
	} else {
		print_help();
	}
	
	
	return 0;
}